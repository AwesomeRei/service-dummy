package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	handleRequests()
}

func homePage(w http.ResponseWriter, r *http.Request) {
	// log.Println(r)
	query := r.URL.Query()
	trxid := ""
	trxstatus := ""
	trxID, present := query["trxid"]
	if present {
		trxid = trxID[0]
	}
	status, present := query["status"]
	if present {
		trxstatus = status[0]
	}
	log.Println(trxid, trxstatus)
	rand.Seed(time.Now().UnixNano())
	min := 0
	max := 500
	number := rand.Intn(max-min+1) + min
	fmt.Println(number)
	time.Sleep(time.Duration(number * int(time.Millisecond)))
	w.WriteHeader(200)
	w.Write([]byte("ok"))
}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("ok"))
}

func handleRequests() {
	http.HandleFunc("/path", homePage)
	http.HandleFunc("/health", healthHandler)
	fmt.Println("serving in :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
